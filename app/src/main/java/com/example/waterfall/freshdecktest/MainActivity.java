package com.example.waterfall.freshdecktest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.freshchat.consumer.sdk.Freshchat;
import com.freshchat.consumer.sdk.FreshchatConfig;
import com.freshchat.consumer.sdk.FreshchatUser;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FreshchatConfig freshchatConfig=new FreshchatConfig("c01ca7b6-3fcf-4b33-8676-73b6115079ec","c83dc340-0f65-4184-9ded-9c968a92d023");
        freshchatConfig.setGallerySelectionEnabled(true);
        Freshchat.getInstance(getApplicationContext()).init(freshchatConfig);
        FreshchatUser user = Freshchat.getInstance(getApplicationContext()).getUser();
        user.setFirstName("John").setLastName("Doe").setEmail("john@john.doe").setPhone("001", "2542542544");
        Freshchat.getInstance(getApplicationContext()).setUser(user);
        Freshchat.showFAQs(this);

    }
}
